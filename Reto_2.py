def ubicarPivote(lista, inf, sup):
    pivote = lista[sup]
    i = inf - 1
 
    for j in range(inf, sup):
        if lista[j] <= pivote: 
            i = i + 1
            (lista[i], lista[j]) = (lista[j], lista[i])

    (lista[i + 1], lista[sup]) = (lista[sup], lista[i + 1])
    return i + 1

def ordenar(array, inferior, superior):
    if inferior < superior:
        pi = ubicarPivote(array, inferior, superior)
        ordenar(array, inferior, pi - 1)
        ordenar(array, pi + 1, superior)

def lista_cuadrados(lista, S):
    listCuadrados = [num ** 2 for num in lista]
    output = []

    for square in listCuadrados:
        if 0 <= square <= int(str(S)+ str(S)):
            output.append(square)

    ordenar(output,0,len(output)-1)    

    return output

# Ejemplos con S=8
numeros = [1, -2, 7,3, 5, 6, 8, 9]
resultado = lista_cuadrados(numeros, 8)
print("Lista original:", numeros)
print("Lista modificada:", resultado)

def ubicarPivote(lista, inf, sup):
    pivote = lista[sup]
    i = inf - 1
 
    for j in range(inf, sup):
        if lista[j] <= pivote: 
            i = i + 1
            (lista[i], lista[j]) = (lista[j], lista[i])

    (lista[i + 1], lista[sup]) = (lista[sup], lista[i + 1])
    return i + 1

def ordenar(array, inferior, superior):
    if inferior < superior:
        pi = ubicarPivote(array, inferior, superior)
        ordenar(array, inferior, pi - 1)
        ordenar(array, pi + 1, superior)

def menor_no_construible(lista):
    ordenar(lista,0,len(lista)-1)
    menor_no_construible = 1

    for num in lista:
        if num > menor_no_construible:
            break
        menor_no_construible += num

    return menor_no_construible

# Ejemplo de uso
numeros = [1,2,4,8]
resultado = menor_no_construible(numeros)
print(f"El menor valor no construible es: {resultado}")

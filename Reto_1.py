def eliminar_digitos(lista, S):
    resultado = []

    for i in range(len(lista)-1,-1,-1):
        num = lista[i]
        nuevo_num = 0
        potencia = 1

        while num > 0:
            digito = num % 10
            num //= 10

            if digito < S:
                nuevo_num += digito * potencia
                potencia *= 10

        if nuevo_num != 0:
            resultado.append(nuevo_num)

    return resultado

# Ejemplo de uso
numeros = [61, 81918, 679, 456, 888, 6, 25,9]
S = 8

resultado = eliminar_digitos(numeros, S)
print("Lista original:", numeros)
print("Lista modificada:", resultado)
